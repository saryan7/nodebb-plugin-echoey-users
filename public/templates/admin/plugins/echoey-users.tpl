<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<form class="form echoey-users manage-users">
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa fa-user"></i> Echoey Users</div>
				<div class="panel-body">
					<div class="search well">
						<label>Search for Jews</label>
						<input class="form-control" id="search-user-name" data-search-type="username" type="text" placeholder="Enter a username to search"/>
						<i class="fa fa-spinner fa-spin hidden"></i>
						<span id="user-notfound-notify" class="label label-danger hide">User not found!</span><br/>
					</div>

					<ul id="users-container" class="hide">
						<!-- BEGIN users -->
						<div class="users-box" data-uid="{users.uid}" data-username="{users.username}" data-userslug="{users.userslug}">
							<div class="user-image">
								<img src="{users.picture}" class="img-thumbnail user-selectable"/>
								<div class="labels">
									<span class="administrator label label-primary <!-- IF !users.administrator -->hide<!-- ENDIF !users.administrator -->">Admin</span>
									<span class="ban label label-danger <!-- IF !users.banned -->hide<!-- ENDIF !users.banned -->">Banned</span>
								</div>
							</div>
							<a href="{config.relative_path}/user/{users.userslug}" target="_blank">{users.username} ({users.uid})</a><br/>
							<button class="make-jew btn btn-primary">Make Jew</button>
						</div>
						<!-- END users -->
					</ul>
					<table id="jews-container" class="mdl-data-table mdl-js-data-table mdl-data-table__cell--non-numeric hide">
						<thead>
							<tr>
								<th class="mdl-data-table__cell--non-numeric">Technical Jews</th>
								<th>Remove</th>
							</tr>
						</thead>
						<tbody>
							<!-- BEGIN jews -->
							<tr data-uid="{jews.uid}" data-username="{jews.username}">
								<td class="mdl-data-table__cell--non-numeric">
									<a href="{config.relative_path}/user/{jews.userslug}" target="_blank">{jews.username} ({jews.uid})</a>
								</td>
								<td>
									<button class="remove-jew mdl-button">
									  <i class="material-icons">clear</i>
									</button>
								</td>
							</tr>
							<!-- END jews -->
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</div>
</div>
