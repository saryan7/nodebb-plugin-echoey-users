define('admin/plugins/echoey-users', function() {
	var EchoeyUsers = {};

	EchoeyUsers.jews = [];

	EchoeyUsers.addJew = function($jew, callback) {
		var uid = $jew.data('uid') + '';

		if (this.jews.indexOf(uid) === -1) {
			this.jews.push(uid);
		}

		callback(this.jews);
	};

	EchoeyUsers.removeJew = function($jew, callback) {
		var uid = $jew.data('uid') + '';

		this.jews = this.jews.filter(function(jew) {
			return jew !== uid;
		});

		callback(this.jews);
	};

	EchoeyUsers.init = function() {
		var self = this,
		    timeoutId = 0,
		    $userSearch = $('#search-user-name'),
		    $users = $('#users-container'),
		    $jews = $('#jews-container tbody'),
		    $notFound = $('#user-notfound-notify'),
		    $spinner = $('.fa-spinner');

		var appendJew = function($user) {
			var jew = {
				uid: $user.data('uid'),
				username: '(((' + $user.data('username') + ')))',
				userslug: $user.data('userslug')
			};

			var data = {
				jews: [jew]
			};

			templates.parse('admin/plugins/echoey-users', 'jews', data, function(html) {
				$jews.append(html);
				$jews.parent().removeClass('hide');
			});
		};

		var searchTimeout = function(callback, ms) {
			if (typeof ms === 'undefined') {
				ms = 250;
			}

			if (timeoutId !== 0) {
				clearTimeout(timeoutId);
				timeoutId = 0;
			}

			timeoutId = setTimeout(callback, ms);
		};

		socket.emit('admin.settings.get', {
			hash: 'echoey-users'
		}, function(err, data) {
			if (err) {
				return app.alertError(err.message);
			}

			if (data.jews) {
				self.jews = data.jews.split(',');
			}

			if (self.jews.length) {
				$jews.parent().removeClass('hide');
			} else {
				$jews.parent().addClass('hide');
			}
		});

		$users.on('click', 'button.make-jew', function(e) {
			e.preventDefault();

			var $user = $(this).parent();

			self.addJew($user, function(jews) {
				socket.emit('admin.settings.set', {
					hash: 'echoey-users',
					values: { jews: jews.join(',') }
				}, function(err) {
					if (err) {
						return app.alertError(err.message);
					}

					appendJew($user);
					$user.remove();

					searchTimeout(function() {
						if ($userSearch.val().length) {
							$userSearch.trigger('keyup');
						}
					});
				});
			});
		});

		$jews.on('click', 'button.remove-jew', function(e) {
			e.preventDefault();

			var $jew = $(this).parents('tr');

			self.removeJew($jew, function(jews) {
				socket.emit('admin.settings.set', {
					hash: 'echoey-users',
					values: { jews: jews.join(',') }
				}, function(err) {
					if (err) {
						return app.alertError(err.message);
					}

					if (jews.length === 0) {
						$jews.parent().addClass('hide');
					}

					$jew.remove();

					searchTimeout(function() {
						if ($userSearch.val().length) {
							$userSearch.trigger('keyup');
						}
					});
				});
			});
		});

		$userSearch.on('keyup', function() {
			var $this = $(this);
			var type =  $this.attr('data-search-type');

			searchTimeout(function() {
				$spinner.removeClass('hidden');

				socket.emit('admin.user.search', {
					searchBy: type,
					query: $this.val()
				}, function(err, data) {
					if (err) {
						return app.alertError(err.message);
					}

					if (data && data.users.length) {
						data.users = data.users.filter(function(user) {
							return self.jews.indexOf(user.uid + '') === -1;
						});
					}

					templates.parse('admin/plugins/echoey-users', 'users', data, function(html) {
						$users.html(html);

						$spinner.addClass('hidden');

						if ($this.val().length === 0) {
							$users.addClass('hide');
							$notFound.addClass('hide');
						} else if (data && data.users.length === 0) {
							$users.addClass('hide');
							$notFound.html('User not found!')
								.show()
								.addClass('label-danger')
								.removeClass('label-success hide');
						} else {
							$users.removeClass('hide');
							$notFound.html(data.users.length + ' user' + (data.users.length > 1 ? 's' : '') + ' found! Search took ' + data.timing + ' ms.')
								.show()
								.addClass('label-success')
								.removeClass('label-danger hide');
						}
					});
				});
			});
		});
	};

	return EchoeyUsers;
});
