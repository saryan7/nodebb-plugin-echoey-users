"use strict";
/* global RELATIVE_PATH, require, config */

$(document).ready(function() {
	var EchoeyUsers = {};

	// Parse jews
	EchoeyUsers.knownJews = config.echoeyUsers.jews;

	if (typeof EchoeyUsers.knownJews === 'string') {
		EchoeyUsers.knownJews = EchoeyUsers.knownJews.split(',');
	}

	// Add jQuery functions for jew detection and echoing
	$.fn.extend({
		isJew: function() {
			if (this.hasClass('technical-jew')) return false;
			
			var uid = this.data('uid') + '';
			var lolYoureAKike = $.inArray(uid, EchoeyUsers.knownJews) !== -1;
			
			if (lolYoureAKike) this.addClass('technical-jew');
			
			return lolYoureAKike;
		},
		echoName: function() {
			var name = this.html();
			name = '(((' + name + ')))';
			this.html(name);
		},
	});

	// Hardcoding this for now, may add admin form field in the future
	EchoeyUsers.judeUrl = 'https://i.imgur.com/n6PgTJj.gif';

	// Jude image
	EchoeyUsers.jude = function(style) {
		var $jude = $('<img>');
		$jude.addClass('jude');
		$jude.attr('src', EchoeyUsers.judeUrl);
		$jude.attr('alt', 'Technical Jew');
		$jude.attr('title', 'Technical Jew');
		$jude.attr('style', style)
		return $jude;
	};

	// Order 1488
	EchoeyUsers.identifyJews = function() {
		// Posts
		$('a[data-uid]').each(function(i, el) {
			var $el = $(el);

			if ($el.isJew()) {
				$el.after(EchoeyUsers.jude('height:25px;margin:0 0 0 3px;'));
				$el.echoName();
			}
		});

		// Profiles
		$('div.avatar[data-uid]').each(function(i, el) {
			var $el = $(el);

			if ($el.isJew()) {
				$el.append(EchoeyUsers.jude('display:block;height:120px;position:absolute;top:4px;left:10px;'));
				
				var $name = $el.parents('div.account').find('h1.fullname');
				$name.echoName();
			}
		});

		// Users list
		$('li.users-box[data-uid]').each(function(i, el) {
			var $el = $(el);

			if ($el.isJew()) {
				var $user = $el.find('div.user-info a');
				$user.before(EchoeyUsers.jude('display:block;width:auto;height:80px;margin:-80px 0 0 15px;'));
				
				var $name = $user.find('a')
				$name.echoName();
			}
		});

		// Group members
		$('table[component="groups/members"] tr[data-uid]').each(function(i, el) {
			var $el = $(el);

			if ($el.isJew()) {
				var $name = $el.find('td.member-name a');
				$name.before(EchoeyUsers.jude('width:auto;height:25px;margin:0 10px 0 0;'));
				$name.echoName();
			}
		});
	};

	$(window).on('action:ajaxify.contentLoaded action:posts.loaded', EchoeyUsers.identifyJews);
});
