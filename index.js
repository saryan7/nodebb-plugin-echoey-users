(function() {
	'use strict';

	var meta = module.parent.require('./meta'),
	    user = module.parent.require('./user'),
	    EchoeyUsers = {
		onLoad: function(params, callback) {
			var render = function(req, res, next) {
				EchoeyUsers.loadJews(function(jews) {
					res.render('admin/plugins/echoey-users', { jews: jews });
				});
			};
			
			params.router.get('/api/admin/plugins/echoey-users', render);
			params.router.get('/admin/plugins/echoey-users',
			                  params.middleware.admin.buildHeader, render);

			callback();
		},
		getConfig: function(config, callback) {
			EchoeyUsers.loadJewUids(function(uids) {
				config.echoeyUsers = {
					jews: uids
				};

				return callback(null, config);
			});
		},
		loadJews: function(callback) {
			meta.settings.get('echoey-users', function(err, options) {
				if (err) {
					return callback([]);
				}

				var uids = EchoeyUsers.parseUids(options);

				if (uids) {
					user.getUsersData(uids, function(err, jews) {
						if (err) {
							return callback([]);
						}

						jews = jews.map(EchoeyUsers.echo);

						callback(jews);
					});
				} else {
					callback([]);
				}
			});
		},
		loadJewUids: function(callback) {
			meta.settings.get('echoey-users', function(err, options) {
				if (err) {
					return callback([]);
				}

				var uids = EchoeyUsers.parseUids(options);

				callback(uids ? uids : []);
			});
		},
		parseUids: function(options) {
			if (!options.jews || options.jews.length === 0) {
				return null;
			}

			return options.jews.split(',');
		},
		echo: function(jew) {
			jew.username = '(((' + jew.username + ')))';
			return jew;
		},
		admin: {
			menu: function(customHeader, callback) {
				customHeader.plugins.push({
					"route": '/plugins/echoey-users',
					"icon": 'fa-user',
					"name": 'Echoey Users'
				});

				callback(null, customHeader);
			}
		}
	};

	module.exports = EchoeyUsers;
})();
