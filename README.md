# Echoey Users

NodeBB Plugin that allows admins to mark echoey users. Install, activate, then go to `/admin/plugins/echoey-users` to get started.

## Installation

    npm i nodebb-plugin-echoey-users
